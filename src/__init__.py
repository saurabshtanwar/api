from flask import Flask, jsonify
from src.auth import auth
from src.blog import blogs
from flask_jwt_extended import JWTManager
import os


def create_app(test_config=None):
    app = Flask(__name__,
                instance_relative_config=True)

    if test_config is None:
        app.config.from_mapping(
            SECRET_KEY=os.environ.get("SECRET_KEY"),
            SQLALCHEMY_DB_URI=os.environ.get(
                "mysql+pymysql://knowledgeenclave_adminfacility:bJv(2n(eUwK~@knowledgeenclave.com/knowledgeenclave_facility"),
            JWT_SECRET_KEY=os.environ.get("JWT_SECRET_KEY")
        )
    else:
        app.config.from_mapping(test_config)

    @app.get("/")
    def index():
        return("Hello World")

    @app.get("/hello")
    def hello():
        return jsonify({"message": "Hello World"})

    JWTManager(app)

    app.register_blueprint(auth)
    app.register_blueprint(blogs)

    return app
