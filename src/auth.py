import email
from os import access
from flask import Blueprint, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from werkzeug.security import check_password_hash, generate_password_hash
import validators
from flask_jwt_extended import JWTManager, create_access_token, create_refresh_token, jwt_required
from flask_jwt_extended import get_jwt_identity

from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT

Base = automap_base()
# engine = create_engine(
#    "mysql+pymysql://root:saurabhsinght@localhost/bnextgen_facility")
engine = create_engine(
    "mysql+pymysql://knowledgeenclave_adminfacility:bJv(2n(eUwK~@knowledgeenclave.com/knowledgeenclave_facility")
Base.prepare(engine, reflect=True)
User = Base.classes.users
session = Session(engine)

auth = Blueprint("auth", __name__, url_prefix="/api/v1/auth")


@auth.post('/register')
def register():
    name = request.json['name']
    email = request.json['email']
    password = request.json['password']
    mobile = request.json['mobile']

    if len(password) < 6:
        return jsonify({"error": "Password is too short"}), HTTP_400_BAD_REQUEST

    if len(name) < 3:
        return jsonify({"error": "Name is too short"}), HTTP_400_BAD_REQUEST

    if not name.isalnum():
        return jsonify({"error": "Name Needs To be alphanumeric"}), HTTP_400_BAD_REQUEST

    if len(mobile) != 10:
        return jsonify({"error": "Enter a Valid Mobile Number"}), HTTP_400_BAD_REQUEST

    if not validators.email(email):
        return jsonify({"error": "Enter a Valid Email"}), HTTP_400_BAD_REQUEST

    email_check = session.query(User).filter(User.email == email)
    email_length = email_check.all()

    if not len(email_length) == 0:
        return jsonify({"error": "Email already exixts"}), HTTP_409_CONFLICT

    mobile_check = session.query(User).filter(User.mobile == mobile)
    mobile_length = mobile_check.all()

    if not len(mobile_length) == 0:
        return jsonify({"error": "mobile no. already exixts"}), HTTP_409_CONFLICT

    pwd_hash = generate_password_hash(password)

    session.add(User(name=name, email=email, password=pwd_hash, mobile=mobile))
    session.commit()

    return jsonify({"messge": "User created",
                    "user": {
                        "name": name,
                        "email": email
                    }}), HTTP_201_CREATED

    return "user created"


@auth.post('/login')
def login():
    email = request.json.get('email', '')
    password = request.json.get('password', '')

    user = session.query(User).filter(User.email == email).first()

    if user:
        is_pass_correct = check_password_hash(user.password, password)
        if is_pass_correct:
            refresh = create_refresh_token(identity=user.id)
            access = create_access_token(identity=user.id)
            return jsonify({
                'user': {
                    'refresh': refresh,
                    'access': access,
                    'name': user.name,
                    'email': user.email
                }
            }), HTTP_200_OK

    return jsonify({"error": "Wrong Credentials"}), HTTP_401_UNAUTHORIZED


@auth.get('/me')
@jwt_required()
def me():
    user_id = get_jwt_identity()
    user = session.query(User).filter(User.id == user_id).first()

    return jsonify({
        "name": user.name,
        "email": user.email
    }), HTTP_200_OK


@auth.get('/token/refresh')
@jwt_required(refresh=True)
def refresh_user_token():
    identity = get_jwt_identity()
    access = create_access_token(identity=identity)

    return jsonify({
        'access': access
    }), HTTP_200_OK
