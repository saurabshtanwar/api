from flask import Blueprint, jsonify

blogs = Blueprint("blogs", __name__, url_prefix="/api/v1/blogs")


@blogs.get('/')
def get_all():
    return {"blogs": []}
