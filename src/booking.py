from flask import Blueprint, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

Base = automap_base()
engine = create_engine(
    "mysql+pymysql://knowledgeenclave_adminfacility:bJv(2n(eUwK~@knowledgeenclave.com/knowledgeenclave_facility")
Base.prepare(engine, reflect=True)
Booking = Base.classes.bookings
session = Session(engine)

bookings = Blueprint("booking", __name__, url_prefix="/api/v1/booking")
